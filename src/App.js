import React, {useState, useEffect} from 'react';
import axios from 'axios'
import './App.css';
import Currency from './currency/Currency'

function App() {
  const [currencies, setCurrencies] = useState([])
  const [search, setSearch] = useState('')

  useEffect(() => {
    axios.get('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false')
    .then(res => {
      setCurrencies(res.data)
      // console.log(res.data)
    })
    .catch(error => console.log(error))
  }, []);

  const handleChange = e => {
    setSearch(e.target.value)
  }

  const filteredCurrencies = currencies.filter(currency => 
    currency.name.toLowerCase().includes(search.toLowerCase())
    )

  return (
    <div className='currency-main-container'>
          <Currency
            key={currency.id} 
            name={currency.name}
            type={currency.symbol}
            image={currency.image}
            price={currency.current_price}
          />
    </div>
  );
}

export default App;


